﻿Console.Write("Enter the height of triangle: ");
int height = int.Parse(Console.ReadLine());

for (int i = 0; i <= height; i++)
{
    for (int j = 0; j <= i; j++)
    {
        Console.Write("*");
    }
    Console.WriteLine();
}

for (int i = 0; i <= height; i++)
{
    Console.WriteLine();
    for (int j = 0; j <= height - i; j++)
    {
        Console.Write("*");
    }
}