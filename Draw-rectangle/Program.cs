﻿Console.Write("Enter the length of rectangle: ");
int length = int.Parse(Console.ReadLine());
Console.Write("Enter the width of rectangle: ");
int width = int.Parse(Console.ReadLine());

for (int i = 1; i <= width; i++)
{
    Console.WriteLine();
    for (int j = 1; j <= length; j++)
    {
        Console.Write("*");
    }
}