﻿Console.Write("Enter number: ");
int n = int.Parse(Console.ReadLine());
if (n == 0 || n == 1)
{
    Console.WriteLine("0 and 1 are not prime");
    return;
}
for (int i = 2; i <= n; i++)
{
    bool checkPrime = true;
    for (int j = 2; j < i; j++)
    {
        if (i % j == 0)
        {
            checkPrime = false;
            break;
        }
    }
    if (checkPrime == true)
    {
        Console.Write(i + " ");
    }
}

