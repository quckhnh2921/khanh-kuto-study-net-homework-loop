﻿using System.Xml.Serialization;
Console.OutputEncoding = System.Text.Encoding.UTF8;
int choice;
do
{
    Console.WriteLine("Chương trình menu: ");
    Console.WriteLine("1. Tính tổng hai số");
    Console.WriteLine("2. Tính lập phương của một số");
    Console.WriteLine("3. Thoát!");
    Console.WriteLine("---------------------------------");
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            Console.Write("Enter first number: ");
            int firstNumber = int.Parse(Console.ReadLine());
            Console.Write("Enter second number: ");
            int secondNumber = int.Parse(Console.ReadLine());
            int sum = firstNumber + secondNumber;
            Console.WriteLine("Sumary of " + firstNumber + " + " + secondNumber + " = " + sum);
            Console.WriteLine("---------------------------------");
            break;
        case 2:
            Console.Write("Enter an integer: ");
            int n = int.Parse(Console.ReadLine());
            int cube = n * n * n;
            Console.WriteLine("Cube of " + n + " = " + cube);
            Console.WriteLine("---------------------------------");
            break;
        default:
            break;
    }
} while (choice < 3);