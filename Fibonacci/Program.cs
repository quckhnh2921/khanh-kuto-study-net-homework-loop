﻿Console.Write("Enter number: ");
int n = int.Parse(Console.ReadLine());
int firstNum = 0;
int secondNum = 1;
for (int i = 0; i < n; i++)
{
    int nextNum = firstNum + secondNum;
    Console.Write(firstNum + " ");
    firstNum = secondNum;
    secondNum = nextNum;
}
