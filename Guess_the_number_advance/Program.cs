﻿using System.Xml.Serialization;


int guessNumber = 0;
int choice = 0;
do
{
    int randomNumber = Random.Shared.Next(0, 100);
    Console.WriteLine("----Guess-The-Number----");
    Console.WriteLine("|    1. Play game      |");
    Console.WriteLine("|    2. Exit           |");
    Console.WriteLine("------------------------");
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            int count = 0;
            do
            {
                Console.Write("Enter the number that you guess: ");
                guessNumber = int.Parse(Console.ReadLine());
                if (guessNumber < randomNumber)
                {
                    Console.WriteLine("Your number is smaller than random number!, Try higher");
                    count++;
                }
                else if (guessNumber > randomNumber)
                {
                    Console.WriteLine("Your number is bigger than random number!, Try smaller");
                    count++;
                }
                else
                {
                    count++;
                    Console.WriteLine("Your number is correct! " + "You guess " + count + " times! " + "The random number is: " + randomNumber);
                }

            } while (guessNumber != randomNumber);
            break;
        case 2:
            Console.WriteLine("GOOD BYE !");
            break;
        default:
            Console.WriteLine("ERROR");
            break;
    }
} while (choice < 2);
