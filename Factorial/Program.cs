﻿using System.Diagnostics.CodeAnalysis;

Console.Write("Enter an integer: ");
int n = int.Parse(Console.ReadLine());
int factorial = 1;
for (int i = 1; i <= n; i++)
{
    factorial *= i;
}
Console.WriteLine("Factorial: " + factorial);